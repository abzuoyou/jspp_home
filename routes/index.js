const router = require('koa-router')(),
      controller = require('../controllers/Home');

//配置路由
router.get('/', controller.index);
router.get('/list/:kw?', controller.list);  //  添加:kw?  /list/后面有没有都可以
router.get('/error', controller.error);     //  页面出错、找不到页面  显示
router.get('*', controller.error);          //  所有匹配不到得跳到404

module.exports = router
