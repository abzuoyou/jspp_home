const CourseTabModel = require('../db/models/courseTab');

class CourseTabService {
  async getCourseTabData () {
    return await CourseTabModel.findAll({
      attributes: {
        exclude: ['cid', 'createdAt', 'updatedAt']
      }
    });
  }
}

module.exports = new CourseTabService();