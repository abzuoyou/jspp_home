const RecomCouseModel = require('../db/models/recomCourse');

class RecomCouseService {
  async getRecomCouseData () {
    return await RecomCouseModel.findAll({
      where: { status: 1 },
      attributes: {
        exclude: ['cid', 'posterUrl', 'createdAt', 'updatedAt']
      }
    })
  }
}


module.exports = new RecomCouseService();