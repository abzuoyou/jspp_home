import { tplReplace, filterData }  from '../utils/tools';
import courseItemTpl from '../templates/courseItem.tpl';
import { URL } from '../config/config';


export default ($) => {
  const $navList = $('.J_navList'),                           //点击
        $courseList = $('.J_courseList'),                     //模板渲染
        $navItem = $navList.children('.nav-item'),            //改变样式
        courseData = $.parseJSON($('#J_courseData').html());  //缓存池

  const init = () => {
    bindEvend();
  }

  function bindEvend () {
    $navList.on('click', '.nav-lk', onNavClick);
  }

  function onNavClick () {
    const $this = $(this),                                  //  this is click
          curIdx = $this.parent().index(),                  //  父parent index
          id = parseInt($this.attr('data-id')),             //  courseTab id
          data = filterData(courseData, id);                //  filter data

    tabChange(curIdx);
    renderList(data);
  }

  function tabChange (index) {
    $navItem.eq(index).addClass('current')
            .siblings().removeClass('current');
  }

  function renderList (data) {
    let list = '';

    data.forEach((item, index) => {
      list += tplReplace(courseItemTpl, {
        href: item.href,
        posterKey: URL.IMG_BASE_URL + item.posterKey,
        courseName: item.courseName,
        priceClass: item.price === '0' ? 'left free' : 'left price',
        price: item.price === '0' ? '免费' : `￥${item.price}`,
        studentCount: item.studentCount
      });
    });

    $courseList.html(list);
  }

  return {
    init
  }
}
