const URL = {
  IMG_BASE_URL: '//tximg.yqjq.xyz/'
}

const CAROUSEL = {
  duration: 5000,
  autoplay: true
}

export {
  URL,
  CAROUSEL
}