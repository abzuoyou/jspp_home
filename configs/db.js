const ENV = require('./env');

module.exports = {
	MYSQL_CONF: {
		base: {
			host: 'localhost',
			dialect: 'mysql',
			pool: {
				max: 5,
				min: 0,
				idle: 10000
			}
		},
		conf: ['txclassdb', 'root', ENV.isPrd ? '82492743Lyq.' : '12345678']
	}
};