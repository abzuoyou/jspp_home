module.exports = {
	keys: {
		ak: 'ls1GhMflrbEUh7V8b4GHnpgWd4DOxI2hgU_Bkg64',
	  sk: 'tsn6vx3cA4CPmdFrQOjI69FjvcGXJtN6KxGyhNfh'
	},
  bucket: {
  	name: 'js-m'
  },
  files: [
    {
    	path: './public/css/index.css',
    	name: 'css/index.css'
    },
    {
    	path: './public/css/list.css',
    	name: 'css/list.css'
    },
    {
    	path: './public/css/error.css',
    	name: 'css/error.css'
    },
    {
    	path: './public/js/index.js',
    	name: 'js/index.js'
    },
    {
    	path: './public/js/list.js',
    	name: 'js/lists.js'
    },
    {
    	path: './public/js/error.js',
    	name: 'js/error.js'
    },
    {
    	path: './public/img/logo-c872049b5cd29849.png',
    	name: 'img/logo-c872049b5cd29849.png'
    }
  ]
}