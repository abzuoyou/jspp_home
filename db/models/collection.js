const seq = require('../db_connect'),
      { STRING, INT } = require('../../configs/db_type');

const Collertion = seq.define('collections', {
  cid: {
    comment: 'colleection ID',
    type: INT,
    allowNull: false,
    unique: true
  },
  title: {
    comment: 'collection title',
    type: STRING,
    allowNull: false,
  },
  info: {
    comment: 'collection information',
    type: STRING,
    allowNull: false
  },
  qqQunLink: {
    comment: 'the link to open QQ communication',
    type: STRING,
    allowNUll: false
  },
  posterUrl: {
    comment: 'poster Image URL',
    type: STRING,
    allowNull: false
  },
  courseIdList: {
    comment: 'the collection of course IDs',
    type: STRING,
    allowNull: false
  },
  posterKey: {
    commnet: 'qiniu poster image name',
    type: STRING,
    allowNull: false
  },
  status: {
    comment: 'collection status',
    type: INT,
    defaultValue: 1,
    allowNull: false
  }
});

module.exports = Collertion;