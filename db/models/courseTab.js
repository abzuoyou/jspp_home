const seq = require('../db_connect'),
      { STRING, INT } = require('../../configs/db_type');

const CourseTab = seq.define('course_tabs', {
  cid: {
    comment: 'course category ID',
    type: INT,
    allowNull: false,
    unique: true
  },
  title: {
    commnet: 'course tab item title text',
    type: STRING,
    allowNull: false
  }
});

module.exports = CourseTab;