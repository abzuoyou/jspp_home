const seq = require('../db_connect'),
      { STRING, INT, TEXT } = require('../../configs/db_type');

const Teacher = seq.define('teachers', {
  cid: {
    comment: 'teacher ID',
    type: STRING,
    allowNull: false,
    unique: true
  },
  href: {
    comment: 'this link to teacher detail page',
    type: STRING,
    allowNull: false
  },
  teacherName: {
    comment: 'teacher name',
    type: STRING,
    allowNull: false
  },
  teacherImg: {
    comment: 'teacher image url',
    type: STRING,
    allowNull: false
  },
  courseCount: {
    comment: 'course count of the teacher',
    type: INT,
    allowNull: false
  },
  studentCount: {
    commnet: 'student count of the teacher',
    type: INT,
    allowNull: false
  },
  intro: {
    commnet: 'teacher introduction',
    type: TEXT,
    allowNull: false
  },
  teacherImgKey: {
    commnet: 'qiniu teacher image name',
    type: STRING,
    allowNull: false
  },
  isStar: {
    comment: 'is the teacher a star teacher?',
    type: INT,
    defaultValue: 0,
    allowNull: false
  },
  status: {
    comment: 'teacher status',
    type: INT,
    defaultValue: 1,
    allowNull: false
  }
});

module.exports = Teacher;