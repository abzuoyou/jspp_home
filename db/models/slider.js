const seq = require('../db_connect'),
      { STRING, INT } = require('../../configs/db_type');

const Slider = seq.define('sliders', {   //建表
  cid: {
    comment: 'course detail page link',
    type: INT(25),
    allowNull: false,
    unique: true
  },
  href: {
  	comment: 'course detail page link',
  	type: STRING,
  	allowNull: false
  },
  imgUrl: {
    comment: 'course image url',
    type: STRING,
    allowNull: false
  },
  title: {
    comment: 'course name',
    type: STRING,
    allowNull: false
  },
  imgKey: {
    comment: 'course qiniu image name',
    type: STRING,
    allowNull: false

  },
  status: {
    comment: 'slider status',
    type: INT,
    defaultValue: 1,
    allowNull: false
  }
})    

module.exports = Slider;